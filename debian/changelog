icingaweb2 (2.8.2-2) UNRELEASED; urgency=medium

  * Team upload.
  * Bump watch file version to 4.
  * Bump Standards-Version to 4.5.1, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 06 Nov 2020 20:04:15 +0100

icingaweb2 (2.8.2-1) unstable; urgency=high

  * Team upload.
  * New upstream release.
    - Fixes CVE-2020-24368.
    (closes: #968833)
  * Update uglifyjs options for 3.10.1.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 22 Aug 2020 07:16:12 +0200

icingaweb2 (2.8.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Add lintian override for national-encoding.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 30 Jun 2020 05:53:23 +0200

icingaweb2 (2.8.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update copyright years for Icinga GmbH.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 09 Jun 2020 05:56:15 +0200

icingaweb2 (2.8.0~rc1-1~exp2) experimental; urgency=medium

  * Team upload.
  * Recommend nagios-images for icon images.
  * Use uglifyjs instead of node-uglify.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 18 Apr 2020 20:11:31 +0200

icingaweb2 (2.8.0~rc1-1~exp1) experimental; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * New upstream release.
  * Bump Standards-Version to 4.5.0, no changes.
  * Update copyright years for Icinga Development Team.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Drop unnecessary dh arguments: --parallel
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 15 Mar 2020 06:30:52 +0100

icingaweb2 (2.7.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.4.1, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 19 Oct 2019 07:35:47 +0200

icingaweb2 (2.7.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 16 Aug 2019 06:37:28 +0200

icingaweb2 (2.7.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.4.0, no changes.
  * Update copyright file, changes:
    - Add Icinga GmbH to copyright holders
    - Change license for JShrink to BSD-3-Clause
    - Update copyright years for Leaf Corcoran
    - Update copyright holders for jQuery
    - Update files for tipsy
  * Update uglify Makefile.
  * Add lintian override for embedded-php-library.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 31 Jul 2019 06:10:12 +0200

icingaweb2 (2.6.3-1) unstable; urgency=medium

  * Team upload.
  * Update gbp.conf to use --source-only-changes by default.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 07 Jul 2019 12:36:22 +0200

icingaweb2 (2.6.3-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop patches, applied/included upstream.
  * Add php-cli dependency to icingaweb2-common for check-syntax.php.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 25 Apr 2019 05:59:55 +0200

icingaweb2 (2.6.2-3) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.3.0, no changes.
  * Fix Icons list in 'Developer Style' documentation.
  * Add upstream patches to fix PHP 7.3 support.
  * Use .maintscript files instead of dpkg-maintscript-helper directly.
  * Update lintian override for normalize.css.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 05 Mar 2019 10:35:01 +0100

icingaweb2 (2.6.2-2) unstable; urgency=medium

  * Team upload.
  * Add patch to fix Uncaught ErrorException with PHP 7.3.
    (closes: #914457)

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 23 Nov 2018 18:36:14 +0100

icingaweb2 (2.6.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.2.1, no changes.
  * Update Homepage to icinga.com.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 23 Nov 2018 09:23:47 +0100

icingaweb2 (2.6.1-1) unstable; urgency=medium

  * New upstream release.
  * Drop autopkgtest to test installability.
  * Add lintian override for testsuite-autopkgtest-missing.
  * Bump Standards-Version to 4.2.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 06 Aug 2018 07:20:45 +0200

icingaweb2 (2.6.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Sort files section in copyrigh file, add public ifont.
  * Bump Standards-Version to 4.1.5, no changes.
  * Drop PHP 7.2 patch, included upstream.
  * Update copyright file, changes:
    - Change license for dompdf to LGPL-3+
    - Update copyright years for Emanuil Rusev
  * Add overrides for package-contains-documentation-outside-usr-share-doc.
  * Update lintian overrides for ifont.
  * Remove executable bit from ifont files.
  * Update lintian override for embedded-javascript-library.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 21 Jul 2018 09:17:41 +0200

icingaweb2 (2.5.3-1) unstable; urgency=medium

  * Team upload.
  * Add php-curl & php-gd to icingaweb2 Recommends.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 08 May 2018 19:03:13 +0200

icingaweb2 (2.5.3-1~exp3) experimental; urgency=medium

  * Team upload.
  * Add upstream patch for PHP 7.2 support.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 08 May 2018 07:56:36 +0200

icingaweb2 (2.5.3-1~exp2) experimental; urgency=medium

  * Team upload.
  * Use dh_install --list-missing.
  * Include dompdf & HTMLPurfifier in icingaweb2 package.
  * Drop obsolete README.source.
  * Drop unused dependencies from icingaweb2 package.
  * Include migrate module in icingaweb2-common.
  * Drop php-htmlpurifier build dependency & test.
  * Remove extra license files.
  * Remove duplicate DejaVu fonts, symlink to packaged files.
  * Update copyright format URL to use HTTPS.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 06 May 2018 19:40:04 +0200

icingaweb2 (2.5.3-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
    (closes: #889507, #878239)
  * Add gbp.conf to use pristine-tar by default.
  * Update copyright file, changes:
    - Update copyright years for Icinga Development Team
    - Use stand-alone license paragraphs
    - Update Source URL to match watch file
  * Restructure control file with cme.
  * Update Vcs-* URLs for Salsa.
  * Update Homepage to icinga.com.
  * Update dev.icinga.org URL for move to GitHub.
  * Use DOMPDF included in Icinga Web 2.
  * Change Priority from extra to optional.
  * Sort rules in order of execution.
  * Enable parallel builds.
  * Add upstream metadata.
  * Add lintian override for embedded-javascript-library.
  * Move rm_conffile from prerm to postrm.
  * Add autopkgtest to test installability.
  * Check DEB_BUILD_OPTIONS for nocheck in dh_auto_test override.
  * Bump Standards-Version to 4.1.4, changes: priority.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 06 May 2018 17:12:24 +0200

icingaweb2 (2.4.1-1) unstable; urgency=medium

  * [21444ed] Add patch 10_revert_dompdf_update
              to allow use of DOMPDF 0.6.x
  * [9bd86b8] New upstream version 2.4.1

 -- Markus Frosch <lazyfrosch@debian.org>  Fri, 20 Jan 2017 14:24:33 +0100

icingaweb2 (2.4.0-1) unstable; urgency=medium

  * [d17f5af] uglify: Fix rule order for older versions of GNU make
  * [36e1fe4] uglify: Don't care about a header
  * [310d027] uglify: Require at least version 2
  * [9aff116] New upstream version 2.4.0
  * [9608ebe] Embed forked version of ZF1 (Closes: #814143)

    ZF1 has been forked as a minimal base library inside Icingaweb2.

    It is maintained as part of Icingaweb2. See README for reference:
    /usr/share/doc/php-icinga/README-ZF1.md

  * [1827003] Minimal icon font installation
  * [dcdfdcf] Move bash-completion to /usr

 -- Markus Frosch <lazyfrosch@debian.org>  Tue, 13 Dec 2016 15:37:14 +0100

icingaweb2 (2.3.4+fix-1) unstable; urgency=medium

  [ Alexander Wirt ]
  * [7fa211e] Imported Upstream version 2.3.4

  [ Markus Frosch ]
  * [f2d0189] Fix merged base for 2.3.4
  * [4c547d8] Include test module into distribution
  * [a9493cd] Fix icingacli breaking dependency for bash-completion update
  * [3f6ff8a] Add proper Javascript minification with uglifyjs

 -- Markus Frosch <lazyfrosch@debian.org>  Mon, 08 Aug 2016 14:15:06 +0200

icingaweb2 (2.3.4-2) unstable; urgency=medium

  * [a7f069b] Re-add www-data to icingaweb2 group (Closes: #831530)
  * [3c63052] Recommend some optional modules (Closes: #831440)
  * [cc5c7b5] Move bash completion to icingacli package (Closes: #830938)
  * [5a3d014] Fix group and permissions of modules directory

 -- Alexander Wirt <formorer@debian.org>  Sun, 17 Jul 2016 10:13:36 +0200

icingaweb2 (2.3.4-1) unstable; urgency=medium

  * Imported Upstream version 2.3.4

 -- Alexander Wirt <formorer@debian.org>  Mon, 20 Jun 2016 19:57:23 +0200

icingaweb2 (2.3.3-1) unstable; urgency=medium

  * [6c8a309] Imported Upstream version 2.3.3

 -- Markus Frosch <lazyfrosch@debian.org>  Fri, 10 Jun 2016 11:06:50 +0200

icingaweb2 (2.3.2-1) unstable; urgency=medium

  * [c1aee5d] Imported Upstream version 2.3.2

 -- Markus Frosch <lazyfrosch@debian.org>  Thu, 28 Apr 2016 18:19:00 +0200

icingaweb2 (2.3.1-1) unstable; urgency=medium

  * [3c80e44] Imported Upstream version 2.3.1
  * [462f48e] Update dependencies for PHP 7.0 (Closes: #821579, #821680)
  * [38bfc08] Depend on php-xml for DOM handling modules

 -- Markus Frosch <lazyfrosch@debian.org>  Tue, 19 Apr 2016 22:27:28 +0200

icingaweb2 (2.2.0-1) unstable; urgency=medium

  * [1d4af5c] Imported Upstream version 2.2.0
  * [fc7c95e] iframe module is now included in icingaweb2 base

 -- Markus Frosch <lazyfrosch@debian.org>  Tue, 01 Mar 2016 09:58:40 +0100

icingaweb2 (2.1.2-3) unstable; urgency=medium

  * [a95a3f5] Update copyright for php dompdf shipped fonts

 -- Markus Frosch <lazyfrosch@debian.org>  Tue, 23 Feb 2016 11:32:40 +0100

icingaweb2 (2.1.2-2) unstable; urgency=medium

  * [672fcae] Split out icingacli and icingaweb2-common as packages
  * [5927c7e] Include translation module
  * [71eca67] Remove 10_html_purifier patch
  * [a0ef32a] Test php-htmlpurifier loading, allow from version 4.3
  * [a029ff6] Update VCS URLs
  * [4de17c5] Remove duplicate adduser in postinst
  * [0f4f2ac] Bump standards version to 3.9.7

 -- Markus Frosch <lazyfrosch@debian.org>  Mon, 22 Feb 2016 13:02:00 +0100

icingaweb2 (2.1.2-1) unstable; urgency=medium

  * [92126c1] Imported Upstream version 2.1.1
  * [bac2ae7] Update html_purifier patch for downwards compatibility
  * [a488312] Update copyright
  * [6abcf98] Imported Upstream version 2.1.2

 -- Markus Frosch <lazyfrosch@debian.org>  Wed, 23 Dec 2015 15:15:40 +0100

icingaweb2 (2.1.0-2) unstable; urgency=medium

  [ Markus Frosch ]
  * [ac348b1] Add patch for HTMLPurifier path loading

 -- Alexander Wirt <formorer@debian.org>  Wed, 18 Nov 2015 11:53:08 +0100

icingaweb2 (2.1.0-1) unstable; urgency=medium

  [ Alexander Wirt ]
  * [44344a0] Its zend-framework on precise
  * [1b1956d] zend version on precise is too old, remove the alternative

  [ Markus Frosch ]
  * [58169fb] Imported Upstream version 2.1.0
  * [f65e92d] Add iframe module

 -- Markus Frosch <lazyfrosch@debian.org>  Mon, 16 Nov 2015 16:29:42 +0100

icingaweb2 (2.0.0-1) unstable; urgency=medium

  [ Markus Frosch ]
  * [bd9e401] Ship empty modules directory in main package
  * [8a9396a] Move setup code into core module

  [ Alexander Wirt ]
  * [a42beb9] Imported Upstream version 2.0.0

 -- Alexander Wirt <formorer@debian.org>  Fri, 02 Oct 2015 14:35:07 +0200

icingaweb2 (2.0.0~rc1-1) unstable; urgency=medium

  * [0ef8c38] Imported Upstream version 2.0.0~rc1

 -- Alexander Wirt <formorer@debian.org>  Fri, 19 Jun 2015 20:37:23 +0200

icingaweb2 (2.0.0~beta3-1) unstable; urgency=low

  [ Markus Frosch ]
  * Initial Debian packaging
  * [d8df5df] Take care about minification of vendor javascripts
  * [458cd2e] Use the yui-compressor for backwards compatibility
  * [6cb0061] Merge icingaweb2-common and icingacli in icingaweb2

  [ Alexander Wirt ]
  * [fa8e336] Imported Upstream version 2.0.0~beta3

 -- Alexander Wirt <formorer@debian.org>  Thu, 19 Mar 2015 20:22:16 +0100
